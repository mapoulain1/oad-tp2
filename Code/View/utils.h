#pragma once


#include <vector>

using namespace std;

static class utils
{
public:
	/// <summary>
	/// G�n�re un random
	/// </summary>
	/// <param name="a">Min</param>
	/// <param name="b">Max</param>
	/// <returns></returns>
	static int random(int a, int b);
	
	/// <summary>
	/// Affiche un vecteur
	/// </summary>
	/// <param name="v">Vecteur</param>
	static void print(std::vector<int> v);
	
	/// <summary>
	/// Affiche deux vecteurs
	/// </summary>
	/// <param name="v1">Vecteur 1</param>
	/// <param name="v2">Vecteur 2</param>
	static void print2(std::vector<int> v1, std::vector<int> v2);

};
