#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include "graph_t.h"

class gestionGraph {
public:
	/// <summary>
	/// Lit un fichier dans un graphe
	/// </summary>
	/// <param name="nom_fichier">Nom du fichier</param>
	/// <returns></returns>
	static graph_t lecture_fichier(std::string nom_fichier);

	/// <summary>
	/// Affiche un graphe
	/// </summary>
	/// <param name="graphe"></param>
	static void affiche_taches(graph_t graphe);

};

