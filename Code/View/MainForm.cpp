#include "MainForm.h"

#include <iostream>
#include <time.h>

#include "bierwirthManager.h"
#include "bierwirth_t.h"
#include "gestion_graphe.h"
#include "solution.h"
#include "utils.h"



graph_t global_graph;
solution_t global_sol;
bierwirth_t global_v;
bool fileLoaded;
bool stopRequested;
char szFile[1024];
vector<string> history;


void MainForm::InitializeComponent(void)
{
	System::Windows::Forms::DataVisualization::Charting::ChartArea^ chartArea2 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
	System::Windows::Forms::DataVisualization::Charting::Series^ series2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
	this->buttonStart = (gcnew System::Windows::Forms::Button());
	this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
	this->label1 = (gcnew System::Windows::Forms::Label());
	this->textBoxIteration = (gcnew System::Windows::Forms::TextBox());
	this->buttonStop = (gcnew System::Windows::Forms::Button());
	this->buttonFile = (gcnew System::Windows::Forms::Button());
	this->textBoxFile = (gcnew System::Windows::Forms::TextBox());
	this->label2 = (gcnew System::Windows::Forms::Label());
	this->label3 = (gcnew System::Windows::Forms::Label());
	this->label4 = (gcnew System::Windows::Forms::Label());
	this->textBoxNbPart = (gcnew System::Windows::Forms::TextBox());
	this->textBoxNbMachine = (gcnew System::Windows::Forms::TextBox());
	this->label5 = (gcnew System::Windows::Forms::Label());
	this->textBoxSupposedBest = (gcnew System::Windows::Forms::TextBox());
	this->textBoxCurrentBest = (gcnew System::Windows::Forms::TextBox());
	this->label6 = (gcnew System::Windows::Forms::Label());
	this->chart = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
	this->listView = (gcnew System::Windows::Forms::ListView());
	this->ListViewFile = (gcnew System::Windows::Forms::ColumnHeader());
	this->ListViewIteration = (gcnew System::Windows::Forms::ColumnHeader());
	this->ListViewCost = (gcnew System::Windows::Forms::ColumnHeader());
	this->label7 = (gcnew System::Windows::Forms::Label());
	this->buttonExport = (gcnew System::Windows::Forms::Button());
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart))->BeginInit();
	this->SuspendLayout();
	// 
	// buttonStart
	// 
	this->buttonStart->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)), static_cast<System::Int32>(static_cast<System::Byte>(78)),
		static_cast<System::Int32>(static_cast<System::Byte>(112)));
	this->buttonStart->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
	this->buttonStart->FlatAppearance->BorderSize = 0;
	this->buttonStart->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
	this->buttonStart->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(0)));
	this->buttonStart->ForeColor = System::Drawing::Color::White;
	this->buttonStart->Location = System::Drawing::Point(12, 12);
	this->buttonStart->Name = L"buttonStart";
	this->buttonStart->Size = System::Drawing::Size(84, 42);
	this->buttonStart->TabIndex = 0;
	this->buttonStart->Text = L"Start";
	this->buttonStart->UseVisualStyleBackColor = false;
	this->buttonStart->Click += gcnew System::EventHandler(this, &MainForm::buttonStart_Click);
	// 
	// openFileDialog1
	// 
	this->openFileDialog1->FileName = L"openFileDialog1";
	this->openFileDialog1->Title = L"File";
	// 
	// label1
	// 
	this->label1->AutoSize = true;
	this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
	this->label1->ForeColor = System::Drawing::Color::White;
	this->label1->Location = System::Drawing::Point(148, 12);
	this->label1->Name = L"label1";
	this->label1->Size = System::Drawing::Size(87, 20);
	this->label1->TabIndex = 1;
	this->label1->Text = L"Iteration :";
	// 
	// textBoxIteration
	// 
	this->textBoxIteration->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)), static_cast<System::Int32>(static_cast<System::Byte>(78)),
		static_cast<System::Int32>(static_cast<System::Byte>(112)));
	this->textBoxIteration->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
	this->textBoxIteration->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
	this->textBoxIteration->ForeColor = System::Drawing::Color::White;
	this->textBoxIteration->Location = System::Drawing::Point(264, 9);
	this->textBoxIteration->Name = L"textBoxIteration";
	this->textBoxIteration->ReadOnly = true;
	this->textBoxIteration->Size = System::Drawing::Size(93, 26);
	this->textBoxIteration->TabIndex = 2;
	// 
	// buttonStop
	// 
	this->buttonStop->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)), static_cast<System::Int32>(static_cast<System::Byte>(78)),
		static_cast<System::Int32>(static_cast<System::Byte>(112)));
	this->buttonStop->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
	this->buttonStop->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
	this->buttonStop->ForeColor = System::Drawing::Color::White;
	this->buttonStop->Location = System::Drawing::Point(12, 60);
	this->buttonStop->Name = L"buttonStop";
	this->buttonStop->Size = System::Drawing::Size(84, 42);
	this->buttonStop->TabIndex = 4;
	this->buttonStop->Text = L"Stop";
	this->buttonStop->UseVisualStyleBackColor = false;
	this->buttonStop->Click += gcnew System::EventHandler(this, &MainForm::buttonStop_Click);
	// 
	// buttonFile
	// 
	this->buttonFile->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)), static_cast<System::Int32>(static_cast<System::Byte>(78)),
		static_cast<System::Int32>(static_cast<System::Byte>(112)));
	this->buttonFile->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
	this->buttonFile->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
	this->buttonFile->ForeColor = System::Drawing::Color::White;
	this->buttonFile->Location = System::Drawing::Point(12, 123);
	this->buttonFile->Name = L"buttonFile";
	this->buttonFile->Size = System::Drawing::Size(122, 42);
	this->buttonFile->TabIndex = 5;
	this->buttonFile->Text = L"Select File";
	this->buttonFile->UseVisualStyleBackColor = false;
	this->buttonFile->Click += gcnew System::EventHandler(this, &MainForm::buttonFile_Click);
	// 
	// textBoxFile
	// 
	this->textBoxFile->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)), static_cast<System::Int32>(static_cast<System::Byte>(78)),
		static_cast<System::Int32>(static_cast<System::Byte>(112)));
	this->textBoxFile->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
	this->textBoxFile->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
	this->textBoxFile->ForeColor = System::Drawing::Color::White;
	this->textBoxFile->Location = System::Drawing::Point(191, 175);
	this->textBoxFile->Name = L"textBoxFile";
	this->textBoxFile->ReadOnly = true;
	this->textBoxFile->Size = System::Drawing::Size(131, 26);
	this->textBoxFile->TabIndex = 7;
	// 
	// label2
	// 
	this->label2->AutoSize = true;
	this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
	this->label2->ForeColor = System::Drawing::Color::White;
	this->label2->Location = System::Drawing::Point(12, 178);
	this->label2->Name = L"label2";
	this->label2->Size = System::Drawing::Size(121, 20);
	this->label2->TabIndex = 6;
	this->label2->Text = L"File selected :";
	// 
	// label3
	// 
	this->label3->AutoSize = true;
	this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
	this->label3->ForeColor = System::Drawing::Color::White;
	this->label3->Location = System::Drawing::Point(12, 204);
	this->label3->Name = L"label3";
	this->label3->Size = System::Drawing::Size(148, 20);
	this->label3->TabIndex = 8;
	this->label3->Text = L"Number of parts :";
	// 
	// label4
	// 
	this->label4->AutoSize = true;
	this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
	this->label4->ForeColor = System::Drawing::Color::White;
	this->label4->Location = System::Drawing::Point(12, 230);
	this->label4->Name = L"label4";
	this->label4->Size = System::Drawing::Size(183, 20);
	this->label4->TabIndex = 9;
	this->label4->Text = L"Number of machines :";
	// 
	// textBoxNbPart
	// 
	this->textBoxNbPart->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)), static_cast<System::Int32>(static_cast<System::Byte>(78)),
		static_cast<System::Int32>(static_cast<System::Byte>(112)));
	this->textBoxNbPart->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
	this->textBoxNbPart->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
	this->textBoxNbPart->ForeColor = System::Drawing::Color::White;
	this->textBoxNbPart->Location = System::Drawing::Point(191, 201);
	this->textBoxNbPart->Name = L"textBoxNbPart";
	this->textBoxNbPart->ReadOnly = true;
	this->textBoxNbPart->Size = System::Drawing::Size(56, 26);
	this->textBoxNbPart->TabIndex = 10;
	// 
	// textBoxNbMachine
	// 
	this->textBoxNbMachine->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)), static_cast<System::Int32>(static_cast<System::Byte>(78)),
		static_cast<System::Int32>(static_cast<System::Byte>(112)));
	this->textBoxNbMachine->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
	this->textBoxNbMachine->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
	this->textBoxNbMachine->ForeColor = System::Drawing::Color::White;
	this->textBoxNbMachine->Location = System::Drawing::Point(191, 227);
	this->textBoxNbMachine->Name = L"textBoxNbMachine";
	this->textBoxNbMachine->ReadOnly = true;
	this->textBoxNbMachine->Size = System::Drawing::Size(56, 26);
	this->textBoxNbMachine->TabIndex = 11;
	// 
	// label5
	// 
	this->label5->AutoSize = true;
	this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
	this->label5->ForeColor = System::Drawing::Color::White;
	this->label5->Location = System::Drawing::Point(12, 256);
	this->label5->Name = L"label5";
	this->label5->Size = System::Drawing::Size(140, 20);
	this->label5->TabIndex = 12;
	this->label5->Text = L"Supposed best :";
	// 
	// textBoxSupposedBest
	// 
	this->textBoxSupposedBest->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)),
		static_cast<System::Int32>(static_cast<System::Byte>(78)), static_cast<System::Int32>(static_cast<System::Byte>(112)));
	this->textBoxSupposedBest->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
	this->textBoxSupposedBest->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
	this->textBoxSupposedBest->ForeColor = System::Drawing::Color::White;
	this->textBoxSupposedBest->Location = System::Drawing::Point(191, 253);
	this->textBoxSupposedBest->Name = L"textBoxSupposedBest";
	this->textBoxSupposedBest->ReadOnly = true;
	this->textBoxSupposedBest->Size = System::Drawing::Size(56, 26);
	this->textBoxSupposedBest->TabIndex = 13;
	// 
	// textBoxCurrentBest
	// 
	this->textBoxCurrentBest->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)),
		static_cast<System::Int32>(static_cast<System::Byte>(78)), static_cast<System::Int32>(static_cast<System::Byte>(112)));
	this->textBoxCurrentBest->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
	this->textBoxCurrentBest->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
	this->textBoxCurrentBest->ForeColor = System::Drawing::Color::White;
	this->textBoxCurrentBest->Location = System::Drawing::Point(264, 38);
	this->textBoxCurrentBest->Name = L"textBoxCurrentBest";
	this->textBoxCurrentBest->ReadOnly = true;
	this->textBoxCurrentBest->Size = System::Drawing::Size(93, 26);
	this->textBoxCurrentBest->TabIndex = 15;
	// 
	// label6
	// 
	this->label6->AutoSize = true;
	this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
	this->label6->ForeColor = System::Drawing::Color::White;
	this->label6->Location = System::Drawing::Point(148, 41);
	this->label6->Name = L"label6";
	this->label6->Size = System::Drawing::Size(118, 20);
	this->label6->TabIndex = 14;
	this->label6->Text = L"Current cost :";
	// 
	// chart
	// 
	this->chart->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)), static_cast<System::Int32>(static_cast<System::Byte>(78)),
		static_cast<System::Int32>(static_cast<System::Byte>(112)));
	chartArea2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)), static_cast<System::Int32>(static_cast<System::Byte>(78)),
		static_cast<System::Int32>(static_cast<System::Byte>(112)));
	chartArea2->BackSecondaryColor = System::Drawing::Color::White;
	chartArea2->Name = L"ChartArea1";
	this->chart->ChartAreas->Add(chartArea2);
	this->chart->Location = System::Drawing::Point(373, 9);
	this->chart->Name = L"chart";
	this->chart->Palette = System::Windows::Forms::DataVisualization::Charting::ChartColorPalette::Light;
	series2->ChartArea = L"ChartArea1";
	series2->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Line;
	series2->IsVisibleInLegend = false;
	series2->Name = L"Series1";
	this->chart->Series->Add(series2);
	this->chart->Size = System::Drawing::Size(432, 300);
	this->chart->TabIndex = 16;
	// 
	// listView
	// 
	this->listView->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)), static_cast<System::Int32>(static_cast<System::Byte>(78)),
		static_cast<System::Int32>(static_cast<System::Byte>(112)));
	this->listView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(3) {
		this->ListViewFile, this->ListViewIteration,
			this->ListViewCost
	});
	this->listView->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
	this->listView->ForeColor = System::Drawing::Color::White;
	this->listView->HeaderStyle = System::Windows::Forms::ColumnHeaderStyle::Nonclickable;
	this->listView->HideSelection = false;
	this->listView->Location = System::Drawing::Point(811, 9);
	this->listView->Name = L"listView";
	this->listView->Size = System::Drawing::Size(304, 300);
	this->listView->TabIndex = 17;
	this->listView->UseCompatibleStateImageBehavior = false;
	this->listView->View = System::Windows::Forms::View::Details;
	// 
	// ListViewFile
	// 
	this->ListViewFile->Text = L"File";
	this->ListViewFile->Width = 100;
	// 
	// ListViewIteration
	// 
	this->ListViewIteration->Text = L"Iteration";
	this->ListViewIteration->Width = 100;
	// 
	// ListViewCost
	// 
	this->ListViewCost->Text = L"Cost";
	this->ListViewCost->Width = 100;
	// 
	// label7
	// 
	this->label7->AutoSize = true;
	this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
	this->label7->ForeColor = System::Drawing::Color::White;
	this->label7->Location = System::Drawing::Point(854, 358);
	this->label7->Name = L"label7";
	this->label7->Size = System::Drawing::Size(261, 20);
	this->label7->TabIndex = 18;
	this->label7->Text = L"Maxime POULAIN et Sinaro LY - F2";
	// 
	// buttonExport
	// 
	this->buttonExport->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(26)), static_cast<System::Int32>(static_cast<System::Byte>(78)),
		static_cast<System::Int32>(static_cast<System::Byte>(112)));
	this->buttonExport->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
	this->buttonExport->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold));
	this->buttonExport->ForeColor = System::Drawing::Color::White;
	this->buttonExport->Location = System::Drawing::Point(932, 315);
	this->buttonExport->Name = L"buttonExport";
	this->buttonExport->Size = System::Drawing::Size(75, 40);
	this->buttonExport->TabIndex = 20;
	this->buttonExport->Text = L"Export";
	this->buttonExport->UseVisualStyleBackColor = false;
	this->buttonExport->Click += gcnew System::EventHandler(this, &MainForm::buttonExport_Click);
	// 
	// MainForm
	// 
	this->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(43)), static_cast<System::Int32>(static_cast<System::Byte>(93)),
		static_cast<System::Int32>(static_cast<System::Byte>(140)));
	this->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
	this->ClientSize = System::Drawing::Size(1123, 387);
	this->Controls->Add(this->buttonExport);
	this->Controls->Add(this->label7);
	this->Controls->Add(this->listView);
	this->Controls->Add(this->chart);
	this->Controls->Add(this->textBoxCurrentBest);
	this->Controls->Add(this->label6);
	this->Controls->Add(this->textBoxSupposedBest);
	this->Controls->Add(this->label5);
	this->Controls->Add(this->textBoxNbMachine);
	this->Controls->Add(this->textBoxNbPart);
	this->Controls->Add(this->label4);
	this->Controls->Add(this->label3);
	this->Controls->Add(this->textBoxFile);
	this->Controls->Add(this->label2);
	this->Controls->Add(this->buttonFile);
	this->Controls->Add(this->buttonStop);
	this->Controls->Add(this->textBoxIteration);
	this->Controls->Add(this->label1);
	this->Controls->Add(this->buttonStart);
	this->Name = L"MainForm";
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart))->EndInit();
	this->ResumeLayout(false);
	this->PerformLayout();

}

System::Void MainForm::buttonFile_Click(System::Object^ sender, System::EventArgs^ e)
{
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = NULL;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = "All\0*.*\0Text\0*.TXT\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	GetOpenFileName(&ofn);

	textBoxFile->Text = charToString(string(szFile).substr(string(szFile).find_last_of("/\\") + 1));

	global_graph = gestionGraph::lecture_fichier(szFile);


	fileLoaded = true;
	stopRequested = false;

	textBoxNbMachine->Text = charToString(to_string(global_graph.nb_machines));
	textBoxNbPart->Text = charToString(to_string(global_graph.nb_pieces));
	textBoxSupposedBest->Text = charToString(to_string(global_graph.supposedBestSolution));
}

String^ MainForm::charToString(char* str)
{
	return gcnew String(str);
}

String^ MainForm::charToString(string str)
{
	return gcnew String(&str[0]);
}

char* MainForm::stringToChar(String^ string)
{
	IntPtr p = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(string);
	char* pNewCharStr = static_cast<char*>(p.ToPointer());
	System::Runtime::InteropServices::Marshal::FreeHGlobal(p);
	return pNewCharStr;
}

System::Void MainForm::buttonStart_Click(System::Object^ sender, System::EventArgs^ e)
{

	if (!fileLoaded) {
		textBoxNbMachine->Text = charToString("Error");
		textBoxNbPart->Text = charToString("Error");
		textBoxSupposedBest->Text = charToString("Error");
		return;
	}
	stopRequested = false;
	global_v = bierwirthManager::generateVector(global_graph.nb_pieces, global_graph.nb_machines);
	global_sol = {};
	global_sol.cost = INT16_MAX;
	textBoxIteration->Text = charToString(to_string(0));
	textBoxCurrentBest->Text = charToString(to_string(0));
	chart->Series["Series1"]->Points->Clear();
	Application::DoEvents();

	solution_t new_solution;
	solution_t best_neighbour;
	bierwirth_t v;
	int nb_neighbours = 0;
	int nb_it = 0;
	int count;
	int size = global_graph.nb_machines * global_graph.nb_pieces;
	bool needToStop = false;
	int needToStopCount;
	int hash[HASH_SIZE] = {};

	while ((nb_it < MAX_IT) && (global_sol.cost > global_graph.supposedBestSolution) && !needToStop && !stopRequested)
	{
		nb_neighbours = 0;
		count = 0;
		needToStop = false;
		needToStopCount = 0;

		best_neighbour.cost = INT_FAST32_MAX;

		do
		{
			v = bierwirthManager::generateVector(global_graph.nb_pieces, global_graph.nb_machines);
			new_solution = solution::rechercheLocale(v, global_graph, 1000);
			new_solution.v = v;
			needToStopCount++;
			if (needToStopCount == MAX_COUNT_HASH) {
				needToStop = true;
			}
		} while (solution::sameH(new_solution, hash, HASH_SIZE) && !needToStop);

		while (nb_neighbours < MAX_NEIGHBOURS && count < MAX_COUNT && nb_neighbours < 1)
		{
			new_solution = solution::genNeighbour(new_solution.v, size, global_graph.nb_pieces, global_graph.nb_machines);
			v = new_solution.v;
			new_solution = solution::rechercheLocale(v, global_graph, 1000);

			new_solution.v = v;

			if (solution::sameH(new_solution, hash, HASH_SIZE) == 0) //s'ils ont un hash diffrent
			{
				++nb_neighbours;
			}
			else
			{
				++count;
			}

			if (new_solution.cost < best_neighbour.cost)
			{
				best_neighbour = new_solution;
			}
		}

		if (best_neighbour.cost < global_sol.cost)
		{
			global_sol = best_neighbour;
		}

		++nb_it;
		textBoxIteration->Text = charToString(to_string(nb_it));
		textBoxCurrentBest->Text = charToString(to_string(global_sol.cost));
		chart->Series["Series1"]->Points->AddXY(nb_it,global_sol.cost);
		Application::DoEvents();
	}

	auto item = gcnew ListViewItem(textBoxFile->Text);

	item->SubItems->Add(charToString(to_string(nb_it)));
	item->SubItems->Add(charToString(to_string(global_sol.cost)));
	listView->Items->Add(item);

	char tmp[256];
	sprintf_s(tmp, "%s, %d, %d", &(string(szFile).substr(string(szFile).find_last_of("/\\")).c_str()[1]), nb_it, global_sol.cost);
	history.push_back(tmp);

}

System::Void MainForm::buttonStop_Click(System::Object^ sender, System::EventArgs^ e)
{
	stopRequested = true;
	global_graph = gestionGraph::lecture_fichier(szFile);

}

System::Void MainForm::buttonExport_Click(System::Object^ sender, System::EventArgs^ e)
{
	ofstream myfile;
	char filename[100];
	sprintf_s(filename, "out_%d.csv", (int)time(NULL));
	myfile.open(filename);
	myfile << "fichier, nb_iteration, meilleure_solution" << std::endl;
	for each (string s in history)
	{
		myfile << s << std::endl;
	}

	myfile.close();
}




