#include "utils.h"
#include <stdlib.h>
#include <iostream>

using namespace std;

int utils::random(int a, int b) {
	return (rand() % (b - a + 1)) + a;
}


void utils::print(std::vector<int> v)
{
	for (size_t i = 0; i < v.size(); i++)
	{
		cout << v[i] << " ";
	}
	cout << endl << endl;
}

void utils::print2(std::vector<int> v1, std::vector<int> v2)
{
	for (size_t i = 0; i < v1.size(); i++)
	{
		cout << i << " : " << v1[i] << " " << v2[i] << endl;
	}
	cout << endl << endl;
}


