#include "bierwirthManager.h"
#include "utils.h"
#include <iostream>

using namespace std;


bierwirth_t bierwirthManager::generateVector(int n, int m) {
	vector<int> vect(n, m);
	vector<int> vectPiece;
	long size = n * m;
	bierwirth_t v(size);

	int maxIteration = n * m;
	int indexRandom;
	int vectMaxIndex = n - 1;
	int vCurrentIndex = 0;

	for (int i = 0; i <= n; i++)
		vectPiece.push_back(i);

	for (int i = 0; i < maxIteration; i++)
	{
		indexRandom = utils::random(0, vectMaxIndex);
		vect[indexRandom]--;
		v[vCurrentIndex] = vectPiece[indexRandom];
		vCurrentIndex++;
		if (vect[indexRandom] == 0) {
			vect[indexRandom] = vect[vectMaxIndex];
			vectPiece[indexRandom] = vectPiece[vectMaxIndex];
			vectMaxIndex--;
		}
	}
	return v;
}

void bierwirthManager::permutation(bierwirth_t& vectorIn, bierwirth_t& vectorOut, pr_t i, pr_t j, int n, int m) {
	int iIndex = prToIndex(vectorIn, i, n, m);
	int jIndex = prToIndex(vectorIn, j, n, m);
	if (iIndex != -1 && jIndex != -1) {
		for (int i = 0; i < n*m; i++)
		{
			vectorOut[i] = vectorIn[i];
		}
		vectorOut[jIndex] = vectorIn[iIndex];
		vectorOut[iIndex] = vectorIn[jIndex];
	}
	else {
		cout << "ERROR PERMUT" << endl;
		cout << "piece : " << i.piece << " rang : " << i.rang << endl;
		cout << "piece : " << j.piece << " rang : " << j.rang << endl;
	}
}



int bierwirthManager::prToIndex(bierwirth_t& vector, pr_t i, int n, int m) {
	int index = n * m - 1;
	int cpt = m ;
	while (index >= 0) {
		if (vector[index] == i.piece)
			cpt--;
		if (cpt == i.rang)
			return index;
		index--;
	}
}




bool bierwirthManager::isVectorValid(bierwirth_t& v, int nbPiece) {
	vector<int> count(nbPiece);
	std::fill(count.begin(), count.end(), 0);
	for (int i = 0; i < v.size(); i++)
	{
		count[v[i]]++;
	}
	bool valid = true;
	for (int i = 1; i < nbPiece; i++)
	{
		if (count[0] != count[i]) {
			valid = false;
			break;
		}
	}
	if (!valid) {
		for (int i = 0; i < nbPiece; i++)
		{
			cout << "\t\t Count " << i << " : " << count[i] << endl;
		}
	}
	return valid;
}


bool bierwirthManager::areVectorsEquals(bierwirth_t& v1, bierwirth_t& v2) {
	if (v1.size() != v2.size())
		return false;
	for (int i = 0; i < v1.size(); i++)
	{
		if (v1[i] != v2[i])
			return false;
	}
	return true;
}