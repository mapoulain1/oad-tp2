#pragma once
#pragma comment(lib, "Comdlg32.lib") 


#include <iostream>

#include <windows.h>
#include <commdlg.h>
#include <msclr\marshal.h>

#include <chrono>
#include <vector>



using namespace std;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

/// <summary>
/// Summary for MainForm
/// </summary>
public ref class MainForm : public System::Windows::Forms::Form
{


public:
	MainForm(void)
	{
		InitializeComponent();
	}

protected:
	/// <summary>
	/// Clean up any resources being used.
	/// </summary>
	~MainForm()
	{
		if (components)
		{
			delete components;
		}
	}
private: System::Windows::Forms::Button^ buttonStart;
protected:

private: System::Windows::Forms::OpenFileDialog^ openFileDialog1;
private: System::Windows::Forms::Label^ label1;
private: System::Windows::Forms::TextBox^ textBoxIteration;




private: System::Windows::Forms::Button^ buttonStop;
private: System::Windows::Forms::Button^ buttonFile;
private: System::Windows::Forms::TextBox^ textBoxFile;
private: System::Windows::Forms::Button^ button2;


private: System::Windows::Forms::Label^ label2;
private: System::Windows::Forms::Label^ label3;
private: System::Windows::Forms::Label^ label4;
private: System::Windows::Forms::TextBox^ textBoxNbPart;
private: System::Windows::Forms::TextBox^ textBoxNbMachine;
private: System::Windows::Forms::Label^ label5;
private: System::Windows::Forms::TextBox^ textBoxSupposedBest;
private: System::Windows::Forms::TextBox^ textBoxCurrentBest;

private: System::Windows::Forms::Label^ label6;
private: System::Windows::Forms::DataVisualization::Charting::Chart^ chart;
private: System::Windows::Forms::ListView^ listView;
private: System::Windows::Forms::ColumnHeader^ ListViewFile;
private: System::Windows::Forms::ColumnHeader^ ListViewIteration;
private: System::Windows::Forms::ColumnHeader^ ListViewCost;
private: System::Windows::Forms::Label^ label7;
private: System::Windows::Forms::Button^ buttonExport;






protected:

private:


	/// <summary>
	/// Required designer variable.
	/// </summary>
	System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	void InitializeComponent(void);
#pragma endregion


private: System::Void buttonFile_Click(System::Object^ sender, System::EventArgs^ e);


private: String^ charToString(char* str);

private: String^ charToString(string str);

private: char* stringToChar(String^ string);


private: System::Void buttonStart_Click(System::Object^ sender, System::EventArgs^ e);

private: System::Void buttonStop_Click(System::Object^ sender, System::EventArgs^ e);

private: System::Void buttonExport_Click(System::Object^ sender, System::EventArgs^ e);


};
