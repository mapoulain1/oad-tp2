#include "gestion_graphe.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

graph_t gestionGraph::lecture_fichier(string nom_fichier)
{
    ifstream monflux(nom_fichier.c_str());
    int nb_pieces;
    int nb_machines;
    int machine;
    int temps;
    graph_t graph;

    monflux >> graph.supposedBestSolution;
    monflux >> nb_pieces;
    monflux >> nb_machines;

    graph.nb_machines = nb_machines;
    graph.nb_pieces = nb_pieces;

    for (int i = 0; i < nb_pieces; i++)
    {
        for (int j = 0; j < nb_machines; j++)
        {
            monflux >> machine;
            monflux >> temps;
            graph.d[i][j] = temps;
            graph.mach[i][j] = machine;
        }
    }

    return graph;
}

void gestionGraph::affiche_taches(graph_t graphe)
{
    cout << "graphe : " << endl;
    cout << "---------------------"<< endl;

    for (int i = 0; i < graphe.nb_pieces; i++)
    {
        cout << "piece : " << i << endl;
        for (int j = 0; j < graphe.nb_machines; j++)
        {
            cout << "machine : " << graphe.mach[i][j] << " cout : " << graphe.d[i][j] << " | ";
        }

        cout << endl;
    }
    cout << "---------------------" << endl;
}