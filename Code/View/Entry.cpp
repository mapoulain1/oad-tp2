#include "MainForm.h"
#include <stdlib.h>



#include "bierwirth_t.h"
#include "bierwirthManager.h"
#include "graph_t.h"
#include "gestion_graphe.h"
#include "solution.h"
#include "utils.h"
#include <iostream>
#include <vector>
#include <chrono>


void testPerf(void) {
	using namespace std;
	graph_t graph;
	char filename[128];
	solution_t solution;

	for (int i = 1; i < 21; i++)
	{
		solution.cost = INT_FAST16_MAX;
		sprintf_s(filename, "la%02d.txt", i);
		graph = gestionGraph::lecture_fichier(filename);
		auto t_start = std::chrono::high_resolution_clock::now();
		solution::grasp(&solution, graph);
		auto t_end = std::chrono::high_resolution_clock::now();
		double elapsed_time_ms = std::chrono::duration<double, std::milli>(t_end - t_start).count();
		cout << filename << " : " << solution.cost << " in " << elapsed_time_ms << " ms" << endl;	
		cin;
	}
}



void test10Seq(void) {
	using namespace std;
	graph_t graph;
	graph = gestionGraph::lecture_fichier("cours.txt");

	solution_t	sol;
	bierwirth_t v[10];


	for (int i = 0; i < 10; i++)
	{
		v[i] = bierwirthManager::generateVector(graph.nb_pieces, graph.nb_machines);
		auto solution = solution::evaluer(v[i], graph);
		auto opti = solution::rechercheLocale(v[i], graph, 1000);
		cout << to_string(i) << " : " << solution.cost << " -> " << opti.cost << endl;
	}
	string in;
	cin >> in;

}





int main() {
	srand(100145);
	//test10Seq();
	//testPerf();

	MainForm form;
	form.ShowDialog();
	return 0;
}