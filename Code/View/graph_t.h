#pragma once
#include "conf.h"

typedef struct
{
	int nb_pieces;
	int nb_machines;
	int supposedBestSolution;

	int mach[GRAPH_MAX_SIZE][GRAPH_MAX_SIZE];
	int d[GRAPH_MAX_SIZE][GRAPH_MAX_SIZE];

} graph_t;