#pragma once


#include "pr.h"
#include <vector>
#include "bierwirth_t.h"

class bierwirthManager {
public:
	/// <summary>
	/// G�n�re un vecteur de Bierwirth
	/// </summary>
	/// <param name="n">Nombre de pi�ces</param>
	/// <param name="m">Nombre de machine</param>
	/// <returns>Vecteur</returns>
	static bierwirth_t generateVector(int n, int m);
	
	/// <summary>
	/// Permute 2 �l�ments dans un vecteur de Bierwirth
	/// </summary>
	/// <param name="vectorIn">Vecteur input</param>
	/// <param name="vectorOut">Vecteur output</param>
	/// <param name="i">Piece 1</param>
	/// <param name="j">Rang 1</param>
	/// <param name="n">Pi�ce 2</param>
	/// <param name="m">Rang 2</param>
	static void permutation(bierwirth_t& vectorIn, bierwirth_t& vectorOut, pr_t i, pr_t j, int n, int m);
	
	/// <summary>
	/// Convertie un couple pi�ce/rang en index
	/// </summary>
	/// <param name="vector">Vecteur</param>
	/// <param name="i">couple</param>
	/// <param name="n">Nombre de pi�ces</param>
	/// <param name="m">Nombre de machines</param>
	/// <returns>Index</returns>
	static int prToIndex(bierwirth_t& vector, pr_t i, int n, int m);
	
	/// <summary>
	/// Test si un vecteur est valid
	/// </summary>
	/// <param name="v">Vecteur</param>
	/// <param name="nbPiece">Nombre pi�ces</param>
	/// <returns></returns>
	static bool isVectorValid(bierwirth_t& v, int nbPiece);
	
	/// <summary>
	/// Test si deux vecteurs sont �gaux
	/// </summary>
	/// <param name="v1">Vecteur 1</param>
	/// <param name="v2">Vecteur 2</param>
	/// <returns></returns>
	static bool areVectorsEquals(bierwirth_t& v1, bierwirth_t& v2);
};
