#include "solution.h"
#include "pr.h"
#include "bierwirthManager.h"
#include <iostream>

using namespace std;

solution_t solution::rechercheLocale(bierwirth_t& v, graph_t& graph, int maxIteration) {
	solution_t solutionEvaluated = evaluer(v, graph);
	solution_t solutionOpti;

	pr_t j = solutionEvaluated.lastFather;
	pr_t jFather = solutionEvaluated.father[j.piece][j.rang];
	bierwirth_t vInput = v;
	bierwirth_t vOpti = v;
	int iteration = 0;
	while (iteration < maxIteration && (jFather.piece != -1 && jFather.rang != -1)) {
		if (graph.mach[j.piece][j.rang] == graph.mach[jFather.piece][jFather.rang]) {
			bierwirthManager::permutation(vInput, vOpti, j, jFather, graph.nb_pieces, graph.nb_machines);
			solutionOpti = evaluer(vOpti, graph);
			if (solutionOpti.cost < solutionEvaluated.cost) {
				vInput = vOpti;
				solutionEvaluated = solutionOpti;
				j = solutionEvaluated.lastFather;
				jFather = solutionEvaluated.father[j.piece][j.rang];
			}
			else {
				j = jFather;
				jFather = solutionEvaluated.father[j.piece][j.rang];
			}
		}
		else {
			j = jFather;
			jFather = solutionEvaluated.father[j.piece][j.rang];
		}
		iteration++;
	}
	return solutionEvaluated;
}

solution_t solution::evaluer(bierwirth_t& v, graph_t& graphe) {
	solution_t sol;
	int machine;
	int n = graphe.nb_pieces;
	int npj;
	int p_prec;
	int pos;
	int j;
	int val;
	sol.cost = -INT_FAST32_MAX;
	int m[SOLUTION_MAX_SIZE][SOLUTION_MAX_SIZE];
	int np[SOLUTION_MAX_SIZE] = { 0 };
	pr_t nm[SOLUTION_MAX_SIZE] = { 0 };

	for (int k = 0; k < n; k++) {
		for (int l = 0; l < graphe.nb_machines; l++) {
			m[k][l] = -INT_FAST32_MAX;
		}

		np[k] = 0;
		nm[k].piece = 0;
		nm[k].rang = 0;
	}

	m[0][0] = 0;

	for (int i = 0; i < v.size(); i++) {
		j = v[i];
		npj = np[j];
		np[j] += 1;
		machine = graphe.mach[j][npj];
		if (npj == 0) {
			m[j][npj] = 0;
			sol.father[j][npj] = { -1, -1 };
		}
		else {
			m[j][npj] = m[j][npj - 1] + graphe.d[j][npj - 1];
			sol.father[j][npj].piece = j;
			sol.father[j][npj].rang = npj - 1;
		}

		if (!(nm[machine].piece == 0 && nm[machine].rang == 0)) {
			p_prec = nm[machine].piece;
			pos = nm[machine].rang;
			val = m[p_prec][pos] + graphe.d[p_prec][pos];

			if (val > m[j][npj]) {
				m[j][npj] = val;
				sol.father[j][npj].piece = p_prec;
				sol.father[j][npj].rang = pos;
			}
		}
		nm[machine].piece = j;
		nm[machine].rang = npj;

		if (npj == graphe.nb_machines - 1) {
			if (m[j][npj] + graphe.d[j][npj] > sol.cost) {
				sol.cost = m[j][npj] + graphe.d[j][npj];
				sol.lastFather.piece = j;
				sol.lastFather.rang = npj;
			}
		}
	}

	pr_t cour = sol.lastFather;
	int i = 0;

	do {
		sol.ST[i] = m[cour.piece][cour.rang];
		cour = sol.father[cour.piece][cour.rang];
		++i;

	} while ((cour.piece >= 0) && (cour.rang >= 0) && (i != 50));

	sol.sol_size = i;

	return sol;
}

void solution::affiche_solution(solution_t sol)
{
	pr_t cour = sol.lastFather;
	int i = 0;
	pr_t chemin[SOLUTION_MAX_SIZE];

	do {
		chemin[i] = cour;
		cour = sol.father[cour.piece][cour.rang];
		++i;

	} while ((cour.piece >= 0) && (cour.rang >= 0) && (i != 50));

	cout << "ordre des taches : " << endl;

	for (int p = i - 1; p >= 0; p--) {
		cout << "piece : " << chemin[p].piece << " | rang : " << chemin[p].rang << " | date de debut : " << sol.ST[p];
		cout << endl;
	}

}

solution_t solution::genNeighbour(bierwirth_t& v, int size, int n, int m)
{
	solution_t new_solution;
	new_solution.v = bierwirthManager::generateVector(n, m);

	int rand1 = rand() % size;
	int rand2 = rand() % size;


	for (int i = 0; i < size; ++i)
	{

		(new_solution.v)[i] = v[i];
	}

	new_solution.v[rand1] = v[rand2];
	new_solution.v[rand2] = v[rand1];

	return new_solution;
}

int solution::sameH(solution_t sol, int* hash, int size)
{
	int tmp;
	int h;
	int sum = 0;
	int val;

	for (int i = 0; i < sol.sol_size; i++)
	{
		tmp = sol.ST[i];
		sum += tmp * tmp;
	}
	h = sum % size;

	val = hash[h];

	if (val == 0)
	{
		hash[h] = 1;
	}

	return val;
}

void solution::grasp(solution_t* sol, graph_t& graphe)
{
	solution_t new_solution;
	solution_t best_neighbour;
	bierwirth_t v;
	int nb_neighbours = 0;
	int nb_it = 0;
	int count;
	int size = graphe.nb_machines * graphe.nb_pieces;
	bool needToStop = false;
	int needToStopCount;
	int hash[HASH_SIZE];

	for (int i = 0; i < HASH_SIZE; i++)
	{
		hash[i] = 0;
	}


	while ((nb_it < MAX_IT) && (sol->cost != graphe.supposedBestSolution) && !needToStop)
	{
		nb_neighbours = 0;
		count = 0;
		needToStop = false;
		needToStopCount = 0;

		best_neighbour.cost = INT_FAST32_MAX;

		do
		{
			v = bierwirthManager::generateVector(graphe.nb_pieces, graphe.nb_machines);
			new_solution = solution::rechercheLocale(v, graphe, 1000);
			new_solution.v = v;
			needToStopCount++;
			if (needToStopCount == MAX_COUNT_HASH) {
				needToStop = true;
			}
		} while (sameH(new_solution, hash, HASH_SIZE) && !needToStop);

		while (nb_neighbours < MAX_NEIGHBOURS && count < MAX_COUNT && nb_neighbours < 1)
		{
			new_solution = genNeighbour(new_solution.v, size, graphe.nb_pieces, graphe.nb_machines);
			v = new_solution.v;
			new_solution = solution::rechercheLocale(v, graphe, 1000);

			new_solution.v = v;

			if (sameH(new_solution, hash, HASH_SIZE) == 0) //s'ils ont un hash diffrent
			{
				++nb_neighbours;
			}
			else
			{
				++count;
			}

			if (new_solution.cost < best_neighbour.cost)
			{
				best_neighbour = new_solution;
			}
		}

		if (best_neighbour.cost < sol->cost)
		{
			*sol = best_neighbour;
		}

		++nb_it;

	}
}




