#pragma once

#include "bierwirth_t.h"
#include "conf.h"
#include "pr.h"

typedef struct {
	bierwirth_t v;
	int ST[SOLUTION_MAX_SIZE];
	pr_t father[SOLUTION_MAX_SIZE][SOLUTION_MAX_SIZE];
	int cost;
	pr_t lastFather;
	int sol_size;
}solution_t;
