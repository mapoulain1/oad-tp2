#pragma once

//Solution
#define SOLUTION_MAX_SIZE 100
#define SOLUTION_MAX_TAB 100
#define HASH_SIZE 90000
#define MAX_NEIGHBOURS 5
#define MAX_COUNT 10
#define MAX_COUNT_HASH 100
#define MAX_IT 300


//Vector
#define MAX_VECTOR_SIZE 200

//Graph
#define GRAPH_MAX_SIZE 30
