#pragma once

#include "bierwirth_t.h"
#include "gestion_graphe.h"
#include "solution_t.h"





class solution
{

public:

	/// <summary>
	/// Recherche locale 
	/// </summary>
	/// <param name="v">Vecteur de bierwirth</param>
	/// <param name="graph">Graphe</param>
	/// <param name="maxIteration">Nombre d'it�ration maximum</param>
	/// <returns>Solution optimis�e</returns>
	static solution_t rechercheLocale(bierwirth_t& v, graph_t& graph, int maxIteration);
	
	/// <summary>
	/// �value un vecteur de Bierwirth sur un graphe
	/// </summary>
	/// <param name="v">Vecteur</param>
	/// <param name="graphe">Graphe</param>
	/// <returns>Solution</returns>
	static solution_t evaluer(bierwirth_t& v, graph_t& graphe);
	
	/// <summary>
	/// Affiche une solution
	/// </summary>
	/// <param name="sol">Solution</param>
	static void affiche_solution(solution_t sol);
	
	/// <summary>
	/// GRASP
	/// </summary>
	/// <param name="sol">Sokution out</param>
	/// <param name="graphe">Graphe</param>
	static void grasp(solution_t* sol, graph_t& graphe);

	
	/// <summary>
	/// G�n�re les voisins pour GRASP
	/// </summary>
	/// <param name="v">Vecteur</param>
	/// <param name="size">Taille</param>
	/// <param name="n">Nombre pi�ce</param>
	/// <param name="m">Nombre machine</param>
	/// <returns></returns>
	static solution_t genNeighbour(bierwirth_t& v, int size, int n, int m);

	/// <summary>
	/// Test le hash du vecteur
	/// </summary>
	/// <param name="sol">Solution</param>
	/// <param name="hash">Tableau de hash</param>
	/// <param name="size">Taille du tableau de hash</param>
	/// <returns></returns>
	static int sameH(solution_t sol, int* hash, int size);


};
