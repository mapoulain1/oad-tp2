# Outil d'aide à la décision - TP2

## Binôme 

Maxime POULAIN

Sinaro LY

## Rapport

[Rapport de TP](Documentation/Rapport%20de%20TP.pdf)

## Executable

[Executable](Executable/x64/View.exe)

<img src="https://i.ibb.co/7rPft4Z/image-2021-11-10-210304.png"/>
